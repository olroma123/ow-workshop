import React from 'react';
import { Link } from "react-router-dom";

const NavItem = (props) => {
  const getActiveClassName = () => {
    if (props.to === window.location.pathname) return `nav__link--active`;
  };

  return (
    <Link className={`nav__link ${getActiveClassName() || ''}`} to={props.to}>
      {props.children}
    </Link>
  );
};

export default NavItem;