import React, { Component } from 'react';
import { withRouter } from 'react-router';
import NavItem from './NavItem';
import './style.scss';

const routes = require('../../routes/nav-main');

class MainNav extends Component {
  render() {
    return (
      <nav className='nav'>
        {routes.map((router, index) => (
          <NavItem key={index} to={router.to}>
            {router.title}
          </NavItem>
        ))}
      </nav>
    );
  }
}

export default withRouter(MainNav);