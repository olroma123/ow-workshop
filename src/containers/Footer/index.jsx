import React, {Component} from 'react';
import { Container, Row, Col } from 'reactstrap';
import IconText from '../../components/Core/IconText';
import { Mail } from 'react-feather';
import './style.scss';

class Footer extends Component {
  render() {
    return (
      <footer>
        <Container fluid style={{height: '100%'}}>
          <Row style={{display: 'flex', alignItems: 'center', height: '100%'}}>
            <Col xs={1}/>
            <Col xs={8}>
              На сайте использованы материалы, принадлежащие Blizzard Entertainment.
            </Col>

            <Col xs={3} className='contacts'>
              Контактная информация:
              <IconText icon={<Mail />}><a href="mailto:olroma123@yandex.ru">olroma123@yandex.ru</a></IconText>
            </Col>
          </Row>
        </Container>
      </footer>
    );
  }
}

export default Footer;