import React, {Component} from 'react';
import { Container, Row, Col } from 'reactstrap';
import Logo from '../../components/Core/Logo';
import './style.scss';

import MainNav from '../MainNav';

class Header extends Component {
  render() {
    return (
      <header>
        <Container fluid>
          <Row>
            <Col xs={1}/>
            <Col xs={2}>
              <Logo />
            </Col>

            <Col xs={7} style={{display: 'flex', alignItems: 'center'}}>
              <MainNav />
            </Col>

            <Col xs={2}>

            </Col>
          </Row>
        </Container>
      </header>
    );
  }
}

export default Header;