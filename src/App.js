import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap-grid.min.css';
import './App.scss';

// ============ Layout components ==============
import Header from './containers/Header';
import Footer from './containers/Footer';

class App extends Component {
  render() {
    return (
      <main className="app">
        <Header />

        <Footer />
      </main>
    );
  }
}

export default App;
