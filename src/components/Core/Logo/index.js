import React from 'react';
import LogoImage from '../../../images/logo.png';
import './style.scss';

const Logo = (props) => {
  return (
    <div className='ow-logo'>
      <img src={LogoImage} alt="ow-workshop-logo"/>
      <span className='ow-logo__title'>OW Workshop</span>
    </div>
  );
};

export default Logo;