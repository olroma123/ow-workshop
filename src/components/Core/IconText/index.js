import React from 'react';
import './style.scss';

const IconText = (props) => {
  return (
    <div className={`icon-text ${props.className || ''}`.trim()}>
      {props.icon}
      {props.text || props.children}
    </div>
  );
};

export default IconText;